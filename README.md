# lurn

[LÖVE](https://love2d.org/) bindings for [Urn](https://urn-lang.com/).

## Usage

There are a few examples in [the `examples` directory](examples).

You can run one with:

```sh
urn examples/<name>.lisp --emit-lua main.lua
love .
```

### Actually using it

Copying [the love directory](love) to your source should let you import it.

Alternatively, you can put it elsewhere and add that directory to Urn's include path when compiling.

### Objects

Each LÖVE class has its own submodule in [the `object` module](love/object) with bindings to its methods.

Methods from superclasses are not re-exported in subclasses, so you may need to import those.

## Alternatives

There's also [Lignum/urn-love2d-bindings](https://gitlab.com/Lignum/urn-love2d-bindings), but it hasn't been updated in several years and might be missing a few features.

## Contributing

If you come across any problems, feel free to open an [issue](https://gitlab.com/wackbyte/lurn/-/issues) or [merge request](https://gitlab.com/wackbyte/lurn/-/merge_requests)!

## License

The [Unlicense](UNLICENSE).
