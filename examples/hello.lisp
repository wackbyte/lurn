"The simplest example."

(import love/event (defevent))
(import love/graphics graphics)

(defevent :draw ()
  (graphics/print! "Hello, world!" 300 200))
