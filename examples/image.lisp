"Demonstrates loading an image and using object methods."

(import love/event (defevent))
(import love/graphics graphics)
(import love/object/texture texture)

(with (image nil)
  (defevent :load ()
    (set! image (graphics/new-image! "examples/image.png"))
    (let* [(width  (texture/get-pixel-width  image))
           (height (texture/get-pixel-height image))]
      (print! ($ "Image dimensions: (~{width}, ~{height})"))))

  (defevent :draw ()
    (graphics/draw! image 200 200)))
