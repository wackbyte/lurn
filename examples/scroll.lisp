"Scrolling text."

(import love/event (defevent))
(import love/graphics graphics)
(import love/object/text object/text)

(define scroll-speed 100)

(let* [(text nil)
       (text-width  nil)
       (text-height nil)
       (x 0)
       (y 0)]

  (defevent :load ()
    (set! text (graphics/new-text (graphics/new-font! 64) "Hello, world!"))
    (set! text-width  (object/text/get-width  text))
    (set! text-height (object/text/get-height text))
    (set! x (- (/ (graphics/get-width)  2) (/ text-width  2)))
    (set! y (- (/ (graphics/get-height) 2) (/ text-height 2))))

  (defevent :update (dt)
    (let* [(screen-width (graphics/get-width))
           (scroll-amount (* dt scroll-speed))]
      (set! x (+ x scroll-amount))
      (cond
        [(< x (- 0 text-width))
         (set! x screen-width)]
        [(> x screen-width)
         (set! x (- 0 text-width))]
        [else])))

  (defevent :draw ()
    (graphics/draw! text x y)))
