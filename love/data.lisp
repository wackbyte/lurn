"See <https://love2d.org/wiki/love.data>."

(define-native compress        :bind-to "love.data.compress")
(define-native decode          :bind-to "love.data.decode")
(define-native decompress      :bind-to "love.data.decompress")
(define-native encode          :bind-to "love.data.encode")
(define-native get-packed-size :bind-to "love.data.getPackedSize")
(define-native hash            :bind-to "love.data.hash")
(define-native new-byte-data   :bind-to "love.data.newByteData")
(define-native new-data-view   :bind-to "love.data.newDataView")
(define-native pack            :bind-to "love.data.pack")
(define-native unpack          :bind-to "love.data.unpack")
