"See <https://love2d.org/wiki/love.event>."

(define-native *love-events*
  "The table builtin event callbacks are stored in."
  :bind-to "love")

(defmacro defevent (name args &body)
  "Define a callback for a builtin event, such as `load`, `update`, `draw`, etc.
  
   See <https://love2d.org/wiki/love> for a full list of callbacks."
  `(.<! *love-events* ,name (lambda ,args ,@body)))

(define-native *love-handlers*
  "The table event handlers are stored in."
  :bind-to "love.handlers")

(defmacro defhandler (name args &body)
  "Define a handler for an event."
  `(.<! *love-handlers* ,name (lambda ,args ,@body)))

(define-native clear! :bind-to "love.event.clear")
(define-native poll   :bind-to "love.event.poll")
(define-native pump!  :bind-to "love.event.pump")
(define-native push!  :bind-to "love.event.push")
(define-native quit!  :bind-to "love.event.quit")
(define-native wait!  :bind-to "love.event.wait")
