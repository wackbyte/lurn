"See <https://love2d.org/wiki/love.font>."

(define-native new-rasterizer!           :bind-to "love.font.newRasterizer")
(define-native new-bm-font-rasterizer!   :bind-to "love.font.newBMFontRasterizer")
(define-native new-glyph-data!           :bind-to "love.font.newGlyphData")
(define-native new-image-rasterizer!     :bind-to "love.font.newImageRasterizer")
(define-native new-true-type-rasterizer! :bind-to "love.font.newTrueTypeRasterizer")
