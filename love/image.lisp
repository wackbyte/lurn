"See <https://love2d.org/wiki/love.image>."

(define-native is-compressed?       :bind-to "love.image.isCompressed")
(define-native new-compressed-data! :bind-to "love.image.newCompressedData")
(define-native new-image-data!      :bind-to "love.image.newImageData")
