"See <https://love2d.org/wiki/love>."

(define-native *version-major*        :bind-to "love._version_major")
(define-native *version-minor*        :bind-to "love._version_minor")
(define-native *version-revision*     :bind-to "love._version_revision")
(define-native get-version            :bind-to "love.getVersion")
(define-native is-version-compatible? :bind-to "love.isVersionCompatible")

(define-native has-deprecation-output? :bind-to "love.hasDeprecationOutput")
(define-native set-deprecation-output! :bind-to "love.setDeprecationOutput")
