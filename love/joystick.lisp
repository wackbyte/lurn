"See <https://love2d.org/wiki/love.joystick>."

(define-native get-gamepad-mapping-string :bind-to "love.joystick.getGamepadMappingString")
(define-native get-joystick-count         :bind-to "love.joystick.getJoystickCount")
(define-native get-joysticks              :bind-to "love.joystick.getJoysticks")
(define-native load-gamepad-mappings!     :bind-to "love.joystick.loadGamepadMappings")
(define-native save-gamepad-mappings!     :bind-to "love.joystick.saveGamepadMappings")
(define-native set-gamepad-mapping!       :bind-to "love.joystick.setGamepadMapping")
