"See <https://love2d.org/wiki/love.keyboard>."

(define-native get-key-from-scancode :bind-to "love.keyboard.getKeyFromScancode")
(define-native get-scancode-from-key :bind-to "love.keyboard.getScancodeFromKey")
(define-native has-key-repeat?       :bind-to "love.keyboard.hasKeyRepeat")
(define-native has-screen-keyboard?  :bind-to "love.keyboard.hasScreenKeyboard")
(define-native has-text-input?       :bind-to "love.keyboard.hasTextInput")
(define-native is-down?              :bind-to "love.keyboard.isDown")
(define-native is-scancode-down?     :bind-to "love.keyboard.isScancodeDown")
(define-native set-key-repeat!       :bind-to "love.keyboard.setKeyRepeat")
(define-native set-text-input!       :bind-to "love.keyboard.setTextInput")
