"See <https://love2d.org/wiki/BezierCurve>."

(import love/object/internal (defwrapper))

(defwrapper evaluate                :evaluate)
(defwrapper get-control-point       :getControlPoint)
(defwrapper get-control-point-count :getControlPointCount)
(defwrapper get-degree              :getDegree)
(defwrapper get-derivative          :getDerivative)
(defwrapper get-segment             :getSegment)
(defwrapper insert-control-point!   :insertControlPoint)
(defwrapper remove-control-point!   :removeControlPoint)
(defwrapper render                  :render)
(defwrapper render-segment          :renderSegment)
(defwrapper rotate!                 :rotate)
(defwrapper scale!                  :scale)
(defwrapper set-control-point!      :setControlPoint)
(defwrapper translate!              :translate)
