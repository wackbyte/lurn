"See <https://love2d.org/wiki/Canvas>."

(import love/object/internal (defwrapper))

(defwrapper generate-mipmaps! :generateMipmaps)
(defwrapper get-msaa          :getMSAA)
(defwrapper get-mipmap-mode   :getMipmapMode)
(defwrapper new-image-data    :newImageData)
(defwrapper render-to!        :renderTo)
