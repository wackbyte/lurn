"See <https://love2d.org/wiki/ChainShape>."

(import love/object/internal (defwrapper))

(defwrapper get-child-edge       :getChildEdge)
(defwrapper get-next-vertex      :getNextVertex)
(defwrapper get-point            :getPoint)
(defwrapper get-points           :getPoints)
(defwrapper get-previous-vertex  :getPreviousVertex)
(defwrapper get-vertex-count     :getVertexCount)
(defwrapper set-next-vertex!     :setNextVertex)
(defwrapper set-previous-vertex! :setPreviousVertex)
