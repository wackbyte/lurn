"See <https://love2d.org/wiki/Channel>."

(import love/object/internal (defwrapper))

(defwrapper clear!          :clear)
(defwrapper demand!         :demand)
(defwrapper get-count       :getCount)
(defwrapper has-read?       :hasRead)
(defwrapper peek            :peek)
(defwrapper perform-atomic! :performAtomic)
(defwrapper pop!            :pop)
(defwrapper push!           :push)
(defwrapper supply!         :supply)
