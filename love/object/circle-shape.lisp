"See <https://love2d.org/wiki/CircleShape>."

(import love/object/internal (defwrapper))

(defwrapper get-point   :getPoint)
(defwrapper get-radius  :getRadius)
(defwrapper set-point!  :setPoint)
(defwrapper set-radius! :setRadius)
