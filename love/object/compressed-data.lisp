"See <https://love2d.org/wiki/CompressedData>."

(import love/object/internal (defwrapper))

(defwrapper get-format :getFormat)
