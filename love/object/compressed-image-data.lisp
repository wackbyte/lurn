"See <https://love2d.org/wiki/CompressedImageData>."

(import love/object/internal (defwrapper))

(defwrapper get-dimensions   :getDimensions)
(defwrapper get-format       :getFormat)
(defwrapper get-height       :getHeight)
(defwrapper get-mipmap-count :getMipmapCount)
(defwrapper get-width        :getWidth)
