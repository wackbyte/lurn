"See <https://love2d.org/wiki/Contact>."

(import love/object/internal (defwrapper))

(defwrapper get-children       :getChildren)
(defwrapper get-fixtures       :getFixtures)
(defwrapper get-friction       :getFriction)
(defwrapper get-normal         :getNormal)
(defwrapper get-positions      :getPositions)
(defwrapper get-restitution    :getRestitution)
(defwrapper is-enabled?        :isEnabled)
(defwrapper is-touching?       :isTouching)
(defwrapper reset-friction!    :resetFriction)
(defwrapper reset-restitution! :resetRestitution)
(defwrapper set-enabled!       :setEnabled)
(defwrapper set-friction!      :setFriction)
(defwrapper set-restitution!   :setRestitution)
