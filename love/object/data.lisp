"See <https://love2d.org/wiki/Data>."

(import love/object/internal (defwrapper))

(defwrapper clone           :clone)
(defwrapper get-ffi-pointer :getFFIPointer)
(defwrapper get-pointer     :getPointer)
(defwrapper get-size        :getSize)
(defwrapper get-string      :getString)
