"See <https://love2d.org/wiki/Decoder>."

(import love/object/internal (defwrapper))

(defwrapper clone             :clone)
(defwrapper decode            :decode)
(defwrapper get-bit-depth     :getBitDepth)
(defwrapper get-channel-count :getChannelCount)
(defwrapper get-channels      :getChannels)
(defwrapper get-duration      :getDuration)
(defwrapper get-sample-rate   :getSampleRate)
(defwrapper seek!             :seek)
