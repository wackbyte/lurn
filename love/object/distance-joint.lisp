"See <https://love2d.org/wiki/DistanceJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-damping-ratio  :getDampingRatio)
(defwrapper get-frequency      :getFrequency)
(defwrapper get-length         :getLength)
(defwrapper set-damping-ratio! :setDampingRatio)
(defwrapper set-frequency!     :setFrequency)
(defwrapper set-length!        :setLength)
