"See <https://love2d.org/wiki/FileData>."

(import love/object/internal (defwrapper))

(defwrapper get-extension :getExtension)
(defwrapper get-filename  :getFilename)
