"See <https://love2d.org/wiki/File>."

(import love/object/internal (defwrapper))

(defwrapper close!       :close)
(defwrapper flush!       :flush)
(defwrapper get-buffer   :getBuffer)
(defwrapper get-filename :getFilename)
(defwrapper get-mode     :getMode)
(defwrapper get-size     :getSize)
(defwrapper is-eof?      :isEOF)
(defwrapper is-open?     :isOpen)
(defwrapper lines!       :lines)
(defwrapper open!        :open)
(defwrapper read!        :read)
(defwrapper seek!        :seek)
(defwrapper set-buffer!  :setBuffer)
(defwrapper tell         :tell)
(defwrapper write!       :write)
