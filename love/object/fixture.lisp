"See <https://love2d.org/wiki/Fixture>."

(import love/object/internal (defwrapper))

(defwrapper destroy!         :destroy)
(defwrapper get-body         :getBody)
(defwrapper get-bounding-box :getBoundingBox)
(defwrapper get-category     :getCategory)
(defwrapper get-density      :getDensity)
(defwrapper get-filter-data  :getFilterData)
(defwrapper get-friction     :getFriction)
(defwrapper get-group-index  :getGroupIndex)
(defwrapper get-mask         :getMask)
(defwrapper get-mass-data    :getMassData)
(defwrapper get-restitution  :getRestitution)
(defwrapper get-shape        :getShape)
(defwrapper get-user-data    :getUserData)
(defwrapper is-destroyed?    :isDestroyed)
(defwrapper is-sensor?       :isSensor)
(defwrapper ray-cast         :rayCast)
(defwrapper set-category!    :setCategory)
(defwrapper set-density!     :setDensity)
(defwrapper set-filter-data! :setFilterData)
(defwrapper set-friction!    :setFriction)
(defwrapper set-group-index! :setGroupIndex)
(defwrapper set-mask!        :setMask)
(defwrapper set-restitution! :setRestitution)
(defwrapper set-sensor!      :setSensor)
(defwrapper set-user-data!   :setUserData)
(defwrapper test-point?      :testPoint)
