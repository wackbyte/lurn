"See <https://love2d.org/wiki/Font>."

(import love/object/internal (defwrapper))

(defwrapper get-ascent       :getAscent)
(defwrapper get-baseline     :getBaseline)
(defwrapper get-dpi-scale    :getDPIScale)
(defwrapper get-descent      :getDescent)
(defwrapper get-filter       :getFilter)
(defwrapper get-height       :getHeight)
(defwrapper get-kerning      :getKerning)
(defwrapper get-line-height  :getLineHeight)
(defwrapper get-width        :getWidth)
(defwrapper get-wrap         :getWrap)
(defwrapper has-glyphs?      :hasGlyphs)
(defwrapper set-fallbacks!   :setFallbacks)
(defwrapper set-filter!      :setFilter)
(defwrapper set-line-height! :setLineHeight)
