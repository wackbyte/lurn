"See <https://love2d.org/wiki/FrictionJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-max-force   :getMaxForce)
(defwrapper get-max-torque  :getMaxTorque)
(defwrapper set-max-force!  :setMaxForce)
(defwrapper set-max-torque! :setMaxTorque)
