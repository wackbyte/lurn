"See <https://love2d.org/wiki/GearJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-joints :getJoints)
(defwrapper get-ratio  :getRatio)
(defwrapper set-ratio! :setRatio)
