"See <https://love2d.org/wiki/GlyphData>."

(import love/object/internal (defwrapper))

(defwrapper get-advance      :getAdvance)
(defwrapper get-bearing      :getBearing)
(defwrapper get-bounding-box :getBoundingBox)
(defwrapper get-dimensions   :getDimensions)
(defwrapper get-format       :getFormat)
(defwrapper get-glyph        :getGlyph)
(defwrapper get-glyph-string :getGlyphString)
(defwrapper get-height       :getHeight)
(defwrapper get-width        :getWidth)
