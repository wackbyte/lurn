"See <https://love2d.org/wiki/ImageData>."

(import love/object/internal (defwrapper))

(defwrapper encode!        :encode)
(defwrapper get-dimensions :getDimensions)
(defwrapper get-format     :getFormat)
(defwrapper get-height     :getHeight)
(defwrapper get-pixel      :getPixel)
(defwrapper get-width      :getWidth)
(defwrapper map-pixel!     :mapPixel)
(defwrapper paste!         :paste)
(defwrapper set-pixel!     :setPixel)
