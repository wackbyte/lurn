"See <https://love2d.org/wiki/Image>."

(import love/object/internal (defwrapper))

(defwrapper is-compressed?    :isCompressed)
(defwrapper is-format-linear? :isFormatLinear)
(defwrapper replace-pixels!   :replacePixels)
