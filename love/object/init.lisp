"See <https://love2d.org/wiki/Object>."

(import love/object/internal (defwrapper))

(defwrapper release! :release)
(defwrapper type     :type)
(defwrapper type-of? :typeOf)
