"Internal utilities."

(defmacro defwrapper (name &meta method)
  "Define a wrapper for a LOVE object method.

   Wrappers take an object for their first argument and pass the remaining arguments (if any) to the method."
  (with (object (gensym 'object))
    `(defun ,name (,object ,'&args)
       ,@meta
       (self ,object ,method @,'args))))
