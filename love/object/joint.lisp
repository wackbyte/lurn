"See <https://love2d.org/wiki/Joint>."

(import love/object/internal (defwrapper))

(defwrapper destroy!              :destroy)
(defwrapper get-anchors           :getAnchors)
(defwrapper get-bodies            :getBodies)
(defwrapper get-collide-connected :getCollideConnected)
(defwrapper get-reaction-force    :getReactionForce)
(defwrapper get-reaction-torque   :getReactionTorque)
(defwrapper get-type              :getType)
(defwrapper get-user-data         :getUserData)
(defwrapper is-destroyed?         :isDestroyed)
(defwrapper set-user-data!        :setUserData)
