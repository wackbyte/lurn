"See <https://love2d.org/wiki/MotorJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-angular-offset  :getAngularOffset)
(defwrapper get-linear-offset   :getLinearOffset)
(defwrapper set-angular-offset! :setAngularOffset)
(defwrapper set-linear-offset!  :setLinearOffset)
