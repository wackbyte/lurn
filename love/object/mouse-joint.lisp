"See <https://love2d.org/wiki/MouseJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-damping-ratio  :getDampingRatio)
(defwrapper get-frequency      :getFrequency)
(defwrapper get-max-force      :getMaxForce)
(defwrapper get-target         :getTarget)
(defwrapper set-damping-ratio! :setDampingRatio)
(defwrapper set-frequency!     :setFrequency)
(defwrapper set-max-force!     :setMaxForce)
(defwrapper set-target!        :setTarget)
