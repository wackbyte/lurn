"See <https://love2d.org/wiki/PolygonShape>."

(import love/object/internal (defwrapper))

(defwrapper get-points :getPoints)
(defwrapper validate?  :validate)
