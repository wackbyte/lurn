"See <https://love2d.org/wiki/PulleyJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-constant       :getConstant)
(defwrapper get-ground-anchors :getGroundAnchors)
(defwrapper get-length-a       :getLengthA)
(defwrapper get-length-b       :getLengthB)
(defwrapper get-ratio          :getRatio)
(defwrapper set-constant!      :setConstant)
(defwrapper set-max-lengths!   :setMaxLengths)
(defwrapper set-ratio!         :setRatio)
