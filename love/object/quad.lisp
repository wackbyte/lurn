"See <https://love2d.org/wiki/Quad>."

(import love/object/internal (defwrapper))

(defwrapper get-texture-dimensions :getTextureDimensions)
(defwrapper get-viewport           :getViewport)
(defwrapper set-viewport!          :setViewport)
