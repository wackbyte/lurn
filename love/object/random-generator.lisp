"See <https://love2d.org/wiki/RandomGenerator>."

(import love/object/internal (defwrapper))

(defwrapper get-seed       :getSeed)
(defwrapper get-state      :getState)
(defwrapper random!        :random)
(defwrapper random-normal! :randomNormal)
(defwrapper set-seed!      :setSeed)
(defwrapper set-state!     :setState)
