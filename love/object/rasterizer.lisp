"See <https://love2d.org/wiki/Rasterizer>."

(import love/object/internal (defwrapper))

(defwrapper get-advance     :getAdvance)
(defwrapper get-ascent      :getAscent)
(defwrapper get-descent     :getDescent)
(defwrapper get-glyph-count :getGlyphCount)
(defwrapper get-glyph-data  :getGlyphData)
(defwrapper get-height      :getHeight)
(defwrapper get-line-height :getLineHeight)
(defwrapper has-glyphs?     :hasGlyphs)
