"See <https://love2d.org/wiki/RecordingDevice>."

(import love/object/internal (defwrapper))

(defwrapper get-bit-depth     :getBitDepth)
(defwrapper get-channel-count :getChannelCount)
(defwrapper get-data          :getData)
(defwrapper get-name          :getName)
(defwrapper get-sample-count  :getSampleCount)
(defwrapper get-sample-rate   :getSampleRate)
(defwrapper is-recording?     :isRecording)
(defwrapper start!            :start)
(defwrapper stop!             :stop)
