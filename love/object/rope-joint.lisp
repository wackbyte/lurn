"See <https://love2d.org/wiki/RopeJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-max-length  :getMaxLength)
(defwrapper set-max-length! :setMaxLength)
