"See <https://love2d.org/wiki/Shader>."

(import love/object/internal (defwrapper))

(defwrapper get-warnings :getWarnings)
(defwrapper has-uniform? :hasUniform)
(defwrapper send!        :send)
(defwrapper send-color!  :sendColor)
