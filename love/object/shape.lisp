"See <https://love2d.org/wiki/Shape>."

(import love/object/internal (defwrapper))

(defwrapper compute-aabb    :computeAABB)
(defwrapper compute-mass    :computeMass)
(defwrapper get-child-count :getChildCount)
(defwrapper get-radius      :getRadius)
(defwrapper get-type        :getType)
(defwrapper ray-cast        :rayCast)
(defwrapper test-point?     :testPoint)
