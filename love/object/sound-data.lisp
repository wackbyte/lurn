"See <https://love2d.org/wiki/SoundData>."

(import love/object/internal (defwrapper))

(defwrapper get-bit-depth     :getBitDepth)
(defwrapper get-channel-count :getChannelCount)
(defwrapper get-channels      :getChannels)
(defwrapper get-duration      :getDuration)
(defwrapper get-sample        :getSample)
(defwrapper get-sample-count  :getSampleCount)
(defwrapper get-sample-rate   :getSampleRate)
(defwrapper set-sample!       :setSample)
