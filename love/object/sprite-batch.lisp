"See <https://love2d.org/wiki/SpriteBatch>."

(import love/object/internal (defwrapper))

(defwrapper add!              :add)
(defwrapper add-layer!        :addLayer)
(defwrapper attach-attribute! :attachAttribute)
(defwrapper clear!            :clear)
(defwrapper flush!            :flush)
(defwrapper get-buffer-size   :getBufferSize)
(defwrapper get-color         :getColor)
(defwrapper get-count         :getCount)
(defwrapper get-texture       :getTexture)
(defwrapper set!              :set)
(defwrapper set-color!        :setColor)
(defwrapper set-draw-range!   :setDrawRange)
(defwrapper set-layer!        :setLayer)
(defwrapper set-texture!      :setTexture)
