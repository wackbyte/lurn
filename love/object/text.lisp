"See <https://love2d.org/wiki/Text>."

(import love/object/internal (defwrapper))

(defwrapper add!           :add)
(defwrapper addf!          :addf)
(defwrapper clear!         :clear)
(defwrapper get-dimensions :getDimensions)
(defwrapper get-font       :getFont)
(defwrapper get-height     :getHeight)
(defwrapper get-width      :getWidth)
(defwrapper set!           :set)
(defwrapper set-font!      :setFont)
(defwrapper setf!          :setf)
