"See <https://love2d.org/wiki/Texture>."

(import love/object/internal (defwrapper))

(defwrapper get-dpi-scale          :getDPIScale)
(defwrapper get-depth              :getDepth)
(defwrapper get-depth-sample-mode  :getDepthSampleMode)
(defwrapper get-dimensions         :getDimensions)
(defwrapper get-filter             :getFilter)
(defwrapper get-format             :getFormat)
(defwrapper get-height             :getHeight)
(defwrapper get-layer-count        :getLayerCount)
(defwrapper get-mipmap-count       :getMipmapCount)
(defwrapper get-mipmap-filter      :getMipmapFilter)
(defwrapper get-pixel-dimensions   :getPixelDimensions)
(defwrapper get-pixel-height       :getPixelHeight)
(defwrapper get-pixel-width        :getPixelWidth)
(defwrapper get-texture-type       :getTextureType)
(defwrapper get-width              :getWidth)
(defwrapper get-wrap               :getWrap)
(defwrapper is-readable?           :isReadable)
(defwrapper set-depth-sample-mode! :setDepthSampleMode)
(defwrapper set-filter!            :setFilter)
(defwrapper set-mipmap-filter!     :setMipmapFilter)
(defwrapper set-wrap!              :setWrap)
