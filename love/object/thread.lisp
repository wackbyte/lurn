"See <https://love2d.org/wiki/Thread>."

(import love/object/internal (defwrapper))

(defwrapper get-error   :getError)
(defwrapper is-running? :isRunning)
(defwrapper start!      :start)
(defwrapper wait!       :wait)
