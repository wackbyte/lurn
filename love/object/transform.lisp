"See <https://love2d.org/wiki/Transform>."

(import love/object/internal (defwrapper))

(defwrapper apply!                  :apply)
(defwrapper clone                   :clone)
(defwrapper get-matrix              :getMatrix)
(defwrapper inverse                 :inverse)
(defwrapper inverse-transform-point :inverseTransformPoint)
(defwrapper is-affine-2d-transform? :isAffine2DTransform)
(defwrapper reset!                  :reset)
(defwrapper rotate!                 :rotate)
(defwrapper scale!                  :scale)
(defwrapper set-matrix!             :setMatrix)
(defwrapper set-transformation!     :setTransformation)
(defwrapper shear!                  :shear)
(defwrapper transform-point         :transformPoint)
(defwrapper translate!              :translate)
