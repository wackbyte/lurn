"See <https://love2d.org/wiki/VideoStream>."

(import love/object/internal (defwrapper))

(defwrapper get-filename :getFilename)
(defwrapper is-playing?  :isPlaying)
(defwrapper pause!       :pause)
(defwrapper play!        :play)
(defwrapper rewind!      :rewind)
(defwrapper seek!        :seek)
(defwrapper set-sync!    :setSync)
(defwrapper tell         :tell)
