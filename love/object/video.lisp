"See <https://love2d.org/wiki/Video>."

(import love/object/internal (defwrapper))

(defwrapper get-dimensions :getDimensions)
(defwrapper get-filter     :getFilter)
(defwrapper get-height     :getHeight)
(defwrapper get-source     :getSource)
(defwrapper get-stream     :getStream)
(defwrapper get-width      :getWidth)
(defwrapper is-playing?    :isPlaying)
(defwrapper pause!         :pause)
(defwrapper play!          :play)
(defwrapper rewind!        :rewind)
(defwrapper seek!          :seek)
(defwrapper set-filter!    :setFilter)
(defwrapper set-source!    :setSource)
(defwrapper tell           :tell)
