"See <https://love2d.org/wiki/WeldJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-damping-ratio   :getDampingRatio)
(defwrapper get-frequency       :getFrequency)
(defwrapper get-reference-angle :getReferenceAngle)
(defwrapper set-damping-ratio!  :setDampingRatio)
(defwrapper set-frequency!      :setFrequency)
