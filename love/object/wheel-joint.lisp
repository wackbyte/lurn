"See <https://love2d.org/wiki/WheelJoint>."

(import love/object/internal (defwrapper))

(defwrapper get-axis                  :getAxis)
(defwrapper get-joint-speed           :getJointSpeed)
(defwrapper get-joint-translation     :getJointTranslation)
(defwrapper get-max-motor-torque      :getMaxMotorTorque)
(defwrapper get-motor-speed           :getMotorSpeed)
(defwrapper get-motor-torque          :getMotorTorque)
(defwrapper get-spring-damping-ratio  :getSpringDampingRatio)
(defwrapper get-spring-frequency      :getSpringFrequency)
(defwrapper is-motor-enabled?         :isMotorEnabled)
(defwrapper set-max-motor-torque!     :setMaxMotorTorque)
(defwrapper set-motor-enabled!        :setMotorEnabled)
(defwrapper set-motor-speed!          :setMotorSpeed)
(defwrapper set-spring-damping-ratio! :setSpringDampingRatio)
(defwrapper set-spring-frequency!     :setSpringFrequency)
