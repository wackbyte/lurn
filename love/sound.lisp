"See <https://love2d.org/wiki/love.sound>."

(define-native new-decoder!    :bind-to "love.sound.newDecoder")
(define-native new-sound-data! :bind-to "love.sound.newSoundData")
