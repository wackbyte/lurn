"See <https://love2d.org/wiki/love.system>."

(define-native get-clipboard-text    :bind-to "love.system.getClipboardText")
(define-native get-os                :bind-to "love.system.getOS")
(define-native get-power-info        :bind-to "love.system.getPowerInfo")
(define-native get-processor-count   :bind-to "love.system.getProcessorCount")
(define-native has-background-music? :bind-to "love.system.hasBackgroundMusic")
(define-native open-url!             :bind-to "love.system.openURL")
(define-native set-clipboard-text!   :bind-to "love.system.setClipboardText")
(define-native vibrate!              :bind-to "love.system.vibrate")
