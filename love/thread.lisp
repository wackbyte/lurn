"See <https://love2d.org/wiki/love.thread>."

(define-native get-channel  :bind-to "love.thread.getChannel")
(define-native new-channel! :bind-to "love.thread.newChannel")
(define-native new-thread!  :bind-to "love.thread.newThread")
