"See <https://love2d.org/wiki/love.timer>."

(define-native get-average-delta :bind-to "love.timer.getAverageDelta")
(define-native get-delta         :bind-to "love.timer.getDelta")
(define-native get-fps           :bind-to "love.timer.getFPS")
(define-native get-time          :bind-to "love.timer.getTime")
(define-native sleep!            :bind-to "love.timer.sleep")
(define-native step!             :bind-to "love.timer.step")
