"See <https://love2d.org/wiki/love.touch>."

(define-native get-position :bind-to "love.touch.getPosition")
(define-native get-pressure :bind-to "love.touch.getPressure")
(define-native get-touches  :bind-to "love.touch.getTouches")
